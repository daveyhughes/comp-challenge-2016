import csv

def readCSV(path, firstCol=False, firstLine=False):
    data = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        if not firstLine: next(reader, None)

        for row in reader:
            line = []
            for index, val in enumerate(row):
                if index or firstCol:
                    line.append(val.replace(" ", ""))
            data.append(line)

    return data

def parseTime(inputTime, style=""):
    hour = int(inputTime[-4:-2])
    day = int(inputTime[:-4])

    options = {
        "day" : day,
        "hour" : hour,
        "index" : (day - 1) * 24 + hour
    }

    if style and type(style) is str:
        output = options[style]
    elif type(style) is list:
        output = []
        for option in style:
            output.append(options[option])
    else:
        output = [day, hour]

    return output

if __name__ == "__main__":
    print (readCSV("../data/sensor-coordinates.txt"))