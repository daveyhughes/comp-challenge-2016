import math
import csv
import re

import customCSV

distPath = "../data/sensor-coordinates.txt"

def distance2d(first, second):
    xDist = abs(second[0] - first[0])
    yDist = abs(second[1] - first[1])

    totalDist = math.sqrt(math.pow(xDist, 2) + math.pow(yDist, 2))

    return totalDist

def listToFloat(inputItem):
    return list(map(lambda item: float(item) if type(item) is str else listToFloat(item), inputItem))

def listToInt(inputItem):
    return list(map(lambda item: int(item) if type(item) is str else listToInt(item), inputItem))

# def listToFloat_2(inputItem):
#     if type(inputItem) is str:
#         return float(inputItem)
#     elif type(inputItem) is list:
#         for index, item in enumerate(inputItem):
#             inputItem[index] = listToFloat_2(item)
#     else:
#         return None
#     return inputItem

def genLinearDist(path=distPath, createCSV=False):
    distMatrix = []

    coordData = listToFloat(customCSV.readCSV(path))

    for index, sensor in enumerate(coordData):
        distLine = []
        for dist in range(0, len(coordData)):
            if dist == index:
                distLine.append(0)
            else:
                # pass
                distLine.append(distance2d(sensor, coordData[dist]))
        distMatrix.append(distLine)

    if createCSV:
        with open("../data/distances.csv", "w") as csvfile:
            writer = csv.writer(csvfile)

            for row in distMatrix:
                writer.writerow(row)

    return distMatrix

if __name__ == "__main__":
    genLinearDist(inputPath)