import customCSV
import distanceMapping
import submission

inPath = "../data/minguBullshit.csv"

def startHereMingu(path=inPath):
    data = distanceMapping.listToInt(customCSV.readCSV(path,True,True)[0])

    submission.genSubmission(data)

if __name__ == "__main__":
    startHereMingu()