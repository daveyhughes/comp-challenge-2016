import csv
import math

import customCSV
import distanceMapping
import submission

dataPath = "../data/sensordata.csv"
queryPath = "../data/test.csv"

def parseSensorData(path=dataPath):
    return customCSV.readCSV(path)

def getOtherData(sensor, day, hour, data=parseSensorData()):
    otherValues = []
    otherDays = []
    otherSensors = []

    hour = hour % 24
    sensorIndex = sensor - 1

    curWeek = int(day / 7)
    dayInWeek = day % 7

    for week in range(0, 5):
        if curWeek != week:
            timeIndex = (week * 7 + dayInWeek) * 24 + hour
            if timeIndex <= len(data[sensorIndex]) - 1:
                otherDays.append(int(data[sensorIndex][timeIndex]))

    otherValues.append(otherDays)

    for x, line in enumerate(data):
        if x != sensorIndex:
            timeIndex = (day - 1) * 24 + hour
            otherSensors.append(data[x][timeIndex])
        else:
            otherSensors.append(0)

    otherValues.append(otherSensors)

    return otherValues

def getQueries(path=queryPath):
    queries = customCSV.readCSV(path, True)

    output = []

    for line in queries:
        curLine = []
        for index, item in enumerate(line):
            if not index:
                curLine.append(int(line[index][1:]))
            if index == 1:
                # print(customCSV.parseTime(line[index], "index"))
                curLine.append(customCSV.parseTime(line[index]))
        output.append(curLine)

    return(output)

def toPercent(inputList):
    curMax = 0
    outList = []

    for sublist in inputList:
        for item in sublist:
            if item > curMax: curMax = item

    for sublist in inputList:
        line = []
        for item in sublist:
            line.append(item / curMax)
        outList.append(line)

    return outList

def guessAlgorithm(curSensor, otherData, distMatrix):
    weights = weightTable[curSensor]
    curDist = distMatrix[curSensor]

    distSkew = .9

    distGuess = 0
    sensorGuess = 0
    finalGuess = 0

    for index, datum in enumerate(otherData[1]):
        weightMapping = math.pow((1 - weights[index]), 1.3) / .45
        distGuess += (int(datum) / (len(otherData[1]) - 1)) * weightMapping

    sensorGuess = sum(otherData[0]) / len(otherData[0])

    finalGuess = (distGuess * distSkew) + (sensorGuess * (1 - distSkew))

    return finalGuess

def genGuesses():
    queries = getQueries()
    distMatrix = distanceMapping.genLinearDist()

    finalGuess = []

    for query in queries:
        curSensor = query[0]
        day = query[1][0]
        hour = query[1][1]

        finalGuess.append(guessAlgorithm(curSensor, getOtherData(curSensor, day, hour), distMatrix))

    submission.genSubmission(finalGuess)

if __name__ == "__main__":
    global weightTable
    weightTable = toPercent(distanceMapping.genLinearDist())
    genGuesses()
