from datetime import datetime
import csv
import random
import os

def genRandSubmission():
    numQueries = 3686
    lower = 12
    upper = 32

    randData = []

    for i in range(0, numQueries):
        randData.append(random.randint(lower, upper))

    genSubmission(randData)

def genSubmission(data):

    timestamp = str(datetime.now())
    filename = timestamp + "_submission.csv"
    header = ["Index", "Count"]

    os.chdir("../submissions")

    with open (filename, "w") as csvfile:
        writer = csv.writer(csvfile)

        writer.writerow(header)

        for index, count in enumerate(data):
            row = [index + 1, count]
            writer.writerow(row)


if __name__ == "__main__":
    genRandSubmission()

